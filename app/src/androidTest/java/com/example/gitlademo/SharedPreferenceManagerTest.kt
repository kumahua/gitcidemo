package com.example.gitlademo

import androidx.test.InstrumentationRegistry
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SharedPreferenceManagerTest {

    @Test
    fun useAppContext() {
//
        // Context of the app under test.
        val appContext = getTargetContext()
        val key = "User_Id"
        val value = "A123456789"

        val sharedPreferenceManager: ISharePreferenceManager = SharedPreferenceManager(appContext)
        sharedPreferenceManager.saveString(key, value)

        val valueFromSP = sharedPreferenceManager.getString(key)

        //將SharedPreference取出，驗證結果
        Assert.assertEquals(value, valueFromSP)
    }
}