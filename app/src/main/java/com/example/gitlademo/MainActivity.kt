package com.example.gitlademo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.example.gitlademo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var v: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v = ActivityMainBinding.inflate(layoutInflater)
        setContentView(v.root)

        v.send.setOnClickListener {

            val loginId = v.loginId.text.toString()
            val pwd = v.password.text.toString()

            val isLoginIdOK = RegisterVerify().isLoginIdVerify(loginId)

            val isPwdOK = RegisterVerify().isPasswordVerify(pwd)


            if (!isLoginIdOK) {

                // 註冊失敗
                val builder = AlertDialog.Builder(this)
                builder.setMessage("帳號至少要6碼，第1碼為英文").setTitle("錯誤")
                builder.show()
            } else if (!isPwdOK) {

                val builder = AlertDialog.Builder(this)
                builder.setMessage("密碼至少要8碼，第1碼為英文，並包含1碼數字").setTitle("錯誤")
                builder.show()
            } else {

                //註冊成功，儲存Id
                Repository(this).saveUserId(loginId)

                val intent = Intent(this, ResultActivity::class.java)
                intent.putExtra("ID", loginId)

                startActivity(intent)
            }
        }
    }
}