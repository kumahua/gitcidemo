package com.example.gitlademo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.gitlademo.databinding.ActivityResultBinding

class ResultActivity : AppCompatActivity() {

    private lateinit var v: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         v = ActivityResultBinding.inflate(layoutInflater)
        setContentView(v.root)

        val id = intent.getStringExtra("ID")
    }
}