package com.example.gitlademo

import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.shadows.ShadowAlertDialog

@RunWith(RobolectricTestRunner::class)
class MainActivityTest {

    private lateinit var activity: MainActivity

    @Before
    fun setupActivity() {

        MockitoAnnotations.initMocks(this)
        activity = Robolectric.buildActivity(MainActivity::class.java).setup().get()
    }

    @Test
    fun registerSuccessShouldDirectToResult() {

        // 需要建立一個ShadowActivity，將用來觀察是否使用startActivity開啟指定的Activity
        val shadowActivity = Shadows.shadowOf(activity)

        val userId = "A123456789"
        val userPassword = "a123456789"
        activity.v.loginId.setText(userId)
        activity.v.password.setText(userPassword)

        activity.v.send.performClick()

        val nextIntent = shadowActivity.nextStartedActivity
        assertEquals(nextIntent.component!!.className, ResultActivity::class.java.name)
        assertEquals(1, nextIntent.extras!!.size())
        assertEquals(userId, nextIntent.extras!!.getString("ID"))
    }

    @Test
    fun registerFailShouldAlert() {

        //arrange
        val userId = "A1234"
        val userPassword = "a123456789"

        activity.v.loginId.setText(userId)
        activity.v.password.setText(userPassword)

        //點下註冊按鈕
        activity.v.send.performClick()

        val dialog = ShadowAlertDialog.getLatestDialog()

        //Assert
        assertNotNull(dialog)
        assertTrue(dialog.isShowing)
    }
}