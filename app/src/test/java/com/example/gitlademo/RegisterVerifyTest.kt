package com.example.gitlademo

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test

class RegisterVerifyTest {

    @Test
    fun isLoginIdVerify() {
        val registerVerify = RegisterVerify()
        assertFalse(registerVerify.isLoginIdVerify("A1234"))
        assertTrue(registerVerify.isLoginIdVerify("A123456"))
    }
}